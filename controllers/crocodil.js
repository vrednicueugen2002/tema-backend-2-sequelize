const CrocodilDb = require("../models").Crocodil;
const InsulaDb = require("../models").Insula;

const controller = {
  getAllCrocodiles: async (req, res) => {
    CrocodilDb.findAll()
      .then((crocodili) => {
        res.status(200).send(crocodili);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Server error!" });
      });
  },

  addCrocodile: async (req, res) => {
    const { idInsula, nume, prenume, varsta } = req.body;
    InsulaDb.findByPk(idInsula)
      .then((insula) => {
        console.log(insula);
        if (insula) {
          insula
            .createCrocodil({ nume, prenume, varsta })
            .then((crocodil) => {
              res.status(201).send(crocodil);
            })
            .catch((err) => {
              console.log(err);
              res.status(500).send({ message: "Server error!" });
            });
        } else {
          res.status(404).send({ message: "Insula not found!" });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Server error!" });
      });
  },

  deleteCrocodile: async (req, res) => {
    const id = req.params.id;
    if (!id) {
      res.status(400).send({ message: "ID not provided" });
    }
    CrocodilDb.destroy({
      where: {
        id: id
      }
    })
    .then((deleteRecord) => {
      if(deleteRecord == 1)
        res.status(200).send({message: "Crocodil sters cu succes"});
      else
        res.status(404).send({message: "Crocodil negasit"});
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({ message: "Server error!" });
});
},
  //aici am vrut sa poti updata si insula din care face parte crocodilul,
  //daca se introduce id-ul unei insule in apelul put atunci se verifica daca exista
  //insula, pentru a trimite crocodilul acolo, iar daca nu se introduce id-ul unei insule
  //se va updata la croco doar ceea ce se trimite in json
  updateCrocodile: async (req, res) => {
    const id = req.params.id;
    const { nume, prenume, varsta, insulaId } = req.body;
    if (!id) {
      res.status(400).send({ message: "ID not provided" });
    }
    InsulaDb.findByPk(insulaId)
    .then((insula) => {
      if (insula || !insulaId) {
        CrocodilDb
      .update({ nume: nume, prenume: prenume, varsta: varsta , insulaId : insulaId}, {
        where: {
          id: id
        }
      })
      .then((updateRecord) => {
        if (updateRecord == 1)
          res.status(202).send({ message: "Crocodil updatat" });
        else
          res.status(404).send({ message: "Crocodil negasit" });
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Server error!" });
      });
      }else
      {
        res.status(404).send({message:"Insula unde vrei sa-l trimiti pe croco nu exista!"});
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({ message: "Server error!" });
    });
    
  },

};

module.exports = controller;
